import { Component, OnInit } from '@angular/core';
import {User} from '../entity/user';
import { LoginService } from '../services/login/login.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

 submitted: boolean = false;
 user: User;
  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
  	this.user = new User("","");
  }

  onSubmit() {
  	this.submitted = true;
    this.loginService.login(this.user)
          .then(x => {
             this.router.navigate(['home']);
          })
          .catch(x => {
              this.router.navigate(['error']);
          })
    
  }



}
