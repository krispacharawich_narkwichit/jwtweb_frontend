import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
import { ErrorComponent } from './error/error.component';

const roots:Routes = [
    {path: '', component: SigninComponent},
	{path: 'signin', component: SigninComponent },
	{path: 'signup', component: SignupComponent},
	{path: 'home', component: HomeComponent },
	{path: 'error', component: ErrorComponent},
	{path: '**', component: SigninComponent}
];
@NgModule({
	
  imports: [
    CommonModule,
    RouterModule.forRoot(roots)
  ],
  exports: [
	RouterModule
	],
  declarations: []
})
export class AppRoutingModule { }
