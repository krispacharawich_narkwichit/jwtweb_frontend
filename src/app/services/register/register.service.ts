import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { User } from '../../entity/user';
import 'rxjs/add/operator/toPromise';
@Injectable()
export class RegisterService {

	private registerUrl = "http://localhost:8080/register";
	private headers = new Headers({'Content-Type':'application/json'});

 constructor(private http: Http) { }

 	register(user : User) {
 		let options = new RequestOptions({headers: this.headers});
 		let promise = new Promise((resolve, reject) => {
 				this.http
 			.post(this.registerUrl, JSON.stringify(user), options)
 			.toPromise()
 			.then(res => resolve(res))
 			.catch(err => reject(err));
 		}) ;
 		return promise;
 	}
}
