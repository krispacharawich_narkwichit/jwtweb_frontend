import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { User } from '../../entity/user';
import 'rxjs/add/operator/toPromise';
@Injectable()
export class LoginService {
	private loginUrl = "http://localhost:8080/login";
	private headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

  constructor(private http: Http) { }

  login(user: User) {  	
  	let options = new RequestOptions({headers:this.headers});
  	let strUser = "username="+user.username+"&password="+user.password+"&submit=Login";
  	let promise = new Promise((resolve, reject) => {
  		 this.http
  			 .post(this.loginUrl,strUser, options)
  			.toPromise()
  			.then(res => { 
  				resolve(res); 
  				})
  			.catch(err => reject(err));
  	});
  	return promise;  	
  }

}
