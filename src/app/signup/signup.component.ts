import { Component, OnInit } from '@angular/core';
import { User } from '../entity/user';
import { RegisterService } from '../services/register/register.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

isSubmitted: boolean = false;
 user: User;
 userSignedIn: User;
  constructor(private registerService: RegisterService
  				, private router: Router) { }

  ngOnInit() {
  	this.user = new User('','');
  }
  onSubmit() {
  	this.registerService.register(this.user)
  		.then(x => this.router.navigate(['signin']));
  }

}
